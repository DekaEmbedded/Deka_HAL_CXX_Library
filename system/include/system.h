#pragma once

// Device header
#include "stm32f411xe.h"

// Device HAL header
#include "stm32f4xx_hal.h"

// using int8_t = char;
using int16_t = short;
using int32_t = long;
using int64_t = long long;

using uint8_t = unsigned char;
using uint16_t = unsigned short;
using uint32_t = unsigned long;
using uint64_t = unsigned long long;
