#pragma once

#include <unordered_map>

#include "dotfactory_struct.h"

class DotFactoryFont : public IFont {
 private:
  const FONT_INFO* m_info;
  uint32_t m_height;

  mutable std::unordered_map<CharType, Bitmap> m_mapOfBitmaps;

 public:
  DotFactoryFont(const FONT_INFO* info, uint32_t height);
  ~DotFactoryFont() override = default;

  Bitmap getCharBitmap(CharType ch) const override;
};
