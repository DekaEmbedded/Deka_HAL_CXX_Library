#include "DotFactoryFont.h"

#define HASH_SIZE 32

DotFactoryFont::DotFactoryFont(const FONT_INFO* info, uint32_t height)
    : m_info(info), m_height(height), m_mapOfBitmaps(HASH_SIZE) {}

Bitmap DotFactoryFont::getCharBitmap(CharType ch) const {
  if (m_info->start_symbol <= ch && ch <= m_info->last_symbol) {
    if (m_mapOfBitmaps.find(ch) == m_mapOfBitmaps.end()) {
      // Need to generate bitmap
      uint32_t desc_address = ch - m_info->start_symbol;
      uint32_t off = m_info->descriptors[desc_address].offset;

      Bitmap bmp = Bitmap(&m_info->bitmaps[off],
                          m_info->descriptors[desc_address].width, m_height);
      m_mapOfBitmaps[ch] = bmp;
    }
    return m_mapOfBitmaps[ch];

  } else {
    return m_mapOfBitmaps['\0'];
  }
}
