#pragma once

#include "../IFont.h"
#include "system.h"

struct FONT_CHAR_INFO {
  uint32_t width;
  uint32_t offset;
};

struct FONT_INFO {
  uint32_t character_height;
  CharType start_symbol;
  CharType last_symbol;
  uint32_t space_character_width;
  const FONT_CHAR_INFO* descriptors;
  const uint8_t* bitmaps;
};
