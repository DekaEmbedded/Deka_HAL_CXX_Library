#pragma once

#include "Bitmap.h"

using CharType = char;

class IFont {
 public:
  IFont() = default;
  virtual ~IFont() = default;

  virtual Bitmap getCharBitmap(CharType ch) const = 0;
};
