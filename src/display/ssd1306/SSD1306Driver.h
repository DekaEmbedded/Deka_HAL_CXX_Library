#pragma once

#include "ISSD1306Connector.h"

enum class SSD1306_DisplayMode { Normal, Inversed };
enum class SSD1306_PowerState { Sleep, Normal };
enum class SSD1306_AddressingMode {
  Horizontal = 0b00,
  Vertical = 0b01,
  PageAddressing = 0b10
};
enum class SSD1306_SegmentMapping { SEG0_To_0_Column, SEG0_To_127_Column };
enum class SSD1306_COMOutputScanMode { Normal, Remapped };
enum class SSD1306_VCOMH_DeselectLevels {
  VCC_MUL_0_DOT_65 = 0b000,
  VCC_MUL_0_DOT_77 = 0b010,
  VCC_MUL_0_DOT_83 = 0b011
};

class SSD1306_Driver {
 private:
  bool m_delete_connector;
  ISSD1306_Connector* m_connector;

  GPIO_TypeDef* m_gpio_reset;
  uint16_t m_gpio_reset_pin;

 public:
  SSD1306_Driver(ISSD1306_Connector* connector, GPIO_TypeDef* gpio_reset,
                 uint16_t gpio_reset_pin,
                 bool delete_connector_when_destory = false);

  ~SSD1306_Driver();

  void setContrastLevel(uint8_t level);
  void entireDisplayOn();
  void resumeDisplayRAMContent();

  void setDisplayMode(SSD1306_DisplayMode mode);
  void setPowerMode(SSD1306_PowerState mode);

  void setLowerColumnStartAddress(uint8_t address);
  void setHigherColumnStartAddress(uint8_t address);
  void setAddressingMode(SSD1306_AddressingMode mode);
  void setColumnStartAndEndAddresses(uint8_t start, uint8_t end);
  void setPageStartAndEndAddresses(uint8_t start, uint8_t end);
  void setPageStartAddress(uint8_t address);

  void setStartLine(uint8_t line);
  void setSegmentMapping(SSD1306_SegmentMapping mode);
  void setMultiplexRatio(uint8_t mux);
  void setCOMOutputScan(SSD1306_COMOutputScanMode mode);
  void setDisplayOffset(uint8_t off);
  void setCOMHardwareConfiguration(bool alternative, bool com_lr_remap);

  void setClockDivideRatioAndOscillatorFreq(uint8_t div, uint8_t freq);
  void setPreChargePeriod(uint8_t phase1, uint8_t phase2);
  void setVCOMH_DeselectLevel(SSD1306_VCOMH_DeselectLevels level);

  void enableChargePump();
  void disableChargePump();

  void sendData(uint8_t* data, uint32_t size);

 private:
  void sendCmd(uint8_t cmd);
};
