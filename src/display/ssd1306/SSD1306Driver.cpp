#include "SSD1306Driver.h"

SSD1306_Driver::SSD1306_Driver(ISSD1306_Connector* connector,
                               GPIO_TypeDef* gpio_reset,
                               uint16_t gpio_reset_pin,
                               bool delete_connector_when_destory)
    : m_delete_connector(delete_connector_when_destory),
      m_connector(connector),
      m_gpio_reset(gpio_reset),
      m_gpio_reset_pin(gpio_reset_pin) {
  HAL_Delay(100);

  GPIO_InitTypeDef init = GPIO_InitTypeDef();
  init.Pin = gpio_reset_pin;
  init.Mode = GPIO_MODE_OUTPUT_PP;

  HAL_GPIO_Init(gpio_reset, &init);
  HAL_GPIO_WritePin(m_gpio_reset, m_gpio_reset_pin, GPIO_PIN_SET);
}

SSD1306_Driver::~SSD1306_Driver() {
  if (m_delete_connector && m_connector) {
    delete m_connector;
    m_connector = nullptr;
  }
  HAL_GPIO_WritePin(m_gpio_reset, m_gpio_reset_pin, GPIO_PIN_RESET);
  HAL_GPIO_DeInit(m_gpio_reset, m_gpio_reset_pin);
}

void SSD1306_Driver::setContrastLevel(uint8_t level) {
  sendCmd(0x81);
  sendCmd(level);
}

void SSD1306_Driver::entireDisplayOn() { sendCmd(0xA5); }

void SSD1306_Driver::resumeDisplayRAMContent() { sendCmd(0xA4); }

void SSD1306_Driver::setDisplayMode(SSD1306_DisplayMode mode) {
  switch (mode) {
    case SSD1306_DisplayMode::Normal:
      sendCmd(0xA6);
      break;
    case SSD1306_DisplayMode::Inversed:
      sendCmd(0xA7);
      break;
  }
}

void SSD1306_Driver::setPowerMode(SSD1306_PowerState mode) {
  switch (mode) {
    case SSD1306_PowerState::Normal:
      sendCmd(0xAF);
      break;
    case SSD1306_PowerState::Sleep:
      sendCmd(0xAE);
      break;
  }
}

void SSD1306_Driver::setLowerColumnStartAddress(uint8_t address) {
  if (0b1111 < address) return;

  sendCmd(0x00 + address);
}

void SSD1306_Driver::setHigherColumnStartAddress(uint8_t address) {
  if (0b1111 < address) return;

  sendCmd(0x10 + address);
}

void SSD1306_Driver::setAddressingMode(SSD1306_AddressingMode mode) {
  sendCmd(0x20);
  sendCmd((uint8_t)mode);
}

void SSD1306_Driver::setColumnStartAndEndAddresses(uint8_t start, uint8_t end) {
  if (127 < start || 127 < end) return;

  sendCmd(0x21);
  sendCmd(start);
  sendCmd(end);
}

void SSD1306_Driver::setPageStartAndEndAddresses(uint8_t start, uint8_t end) {
  if (0b111 < start || 0b111 < end) return;

  sendCmd(0x22);
  sendCmd(start);
  sendCmd(end);
}

void SSD1306_Driver::setPageStartAddress(uint8_t address) {
  if (0b111 < address) return;

  sendCmd(0xB0 + address);
}

void SSD1306_Driver::setStartLine(uint8_t line) {
  if (63 < line) return;

  sendCmd(0x40 + line);
}

void SSD1306_Driver::setSegmentMapping(SSD1306_SegmentMapping mode) {
  switch (mode) {
    case SSD1306_SegmentMapping::SEG0_To_0_Column:
      sendCmd(0xA0);
      break;
    case SSD1306_SegmentMapping::SEG0_To_127_Column:
      sendCmd(0xA1);
      break;
  }
}

void SSD1306_Driver::setMultiplexRatio(uint8_t mux) {
  if (16 <= mux && mux <= 64) {
    sendCmd(0xA8);
    sendCmd(mux - 1);
  }
}

void SSD1306_Driver::setCOMOutputScan(SSD1306_COMOutputScanMode mode) {
  switch (mode) {
    case SSD1306_COMOutputScanMode::Normal:
      sendCmd(0xC0);
      break;
    case SSD1306_COMOutputScanMode::Remapped:
      sendCmd(0xC8);
      break;
  }
}

void SSD1306_Driver::setDisplayOffset(uint8_t off) {
  if (63 < off) return;

  sendCmd(0xD3);
  sendCmd(off);
}

void SSD1306_Driver::setCOMHardwareConfiguration(bool alternative,
                                                 bool com_lr_remap) {
  uint8_t d = 0;
  if (alternative) {
    d |= (1 << 4);
  }
  if (com_lr_remap) {
    d |= (1 << 5);
  }

  sendCmd(0xDA);
  sendCmd(d);
}

void SSD1306_Driver::setClockDivideRatioAndOscillatorFreq(uint8_t div,
                                                          uint8_t freq) {
  if (16 <= div || 0b1111 < freq) return;

  uint8_t d = 0;
  d |= (div - 1);
  d |= (freq << 4);

  sendCmd(0xD5);
  sendCmd(d);
}

void SSD1306_Driver::setPreChargePeriod(uint8_t phase1, uint8_t phase2) {
  if (0b1111 < phase1 || 0b1111 < phase2) return;
  uint8_t d = 0;
  d |= phase1;
  d |= (phase2 << 4);

  sendCmd(0xD9);
  sendCmd(d);
}

void SSD1306_Driver::setVCOMH_DeselectLevel(
    SSD1306_VCOMH_DeselectLevels level) {
  sendCmd(0xDB);
  sendCmd(((uint8_t)level) << 4);
}

void SSD1306_Driver::sendData(uint8_t* data, uint32_t size) {
  if (m_connector) m_connector->send_data(data, size);
}

void SSD1306_Driver::sendCmd(uint8_t cmd) {
  if (m_connector) m_connector->send_cmd(cmd);
}

void SSD1306_Driver::enableChargePump() {
  sendCmd(0x8D);
  sendCmd(0x14);
}

void SSD1306_Driver::disableChargePump() {
  sendCmd(0x8D);
  sendCmd(0x10);
}
