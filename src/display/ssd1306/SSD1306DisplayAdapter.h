#pragma once

#include "../IDisplayAdapter.h"
#include "SSD1306Driver.h"

class SSD1306DisplayAdapter : public IDisplayAdapter {
 private:
  SSD1306_Driver* m_driver;
  bool m_destroy_driver;

  static const uint32_t DisplayWidth = 128;
  static const uint32_t DisplayHeight = 64;
  static const uint32_t BufferSize = DisplayWidth * DisplayHeight / 8;

  uint8_t m_drawBuffer[BufferSize];

 public:
  SSD1306DisplayAdapter(SSD1306_Driver* drv, bool destroy_driver = false);
  ~SSD1306DisplayAdapter() override;

  uint32_t getHeight() const override;
  void swap() override;
  void fill(uint32_t color) override;
  void drawPixel(uint32_t x, uint32_t y, uint32_t color) override;
  uint32_t getWidth() const override;
};
