#pragma once

#include "ISSD1306Connector.h"
#include "system.h"

class SSD1306_I2C_Connector : public ISSD1306_Connector {
 private:
  I2C_HandleTypeDef* m_hnd;
  uint8_t m_devAddress;

 public:
  SSD1306_I2C_Connector(I2C_HandleTypeDef* i2c_handle,
                        bool sa0_enabled = false);
  ~SSD1306_I2C_Connector() override;

  virtual bool send_cmd(uint8_t cmd) override;
  virtual bool send_data(uint8_t* data, uint32_t size) override;

  bool isConnected();
};
