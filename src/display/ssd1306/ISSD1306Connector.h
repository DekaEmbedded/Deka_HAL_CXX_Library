#pragma once

#include "system.h"

class ISSD1306_Connector {
 public:
  ISSD1306_Connector() = default;
  virtual ~ISSD1306_Connector() = default;

  virtual bool send_cmd(uint8_t cmd) = 0;
  virtual bool send_data(uint8_t* data, uint32_t size) = 0;
};
