#include "SSD1306I2CConnector.h"

SSD1306_I2C_Connector::SSD1306_I2C_Connector(I2C_HandleTypeDef* i2c_handle,
                                             bool sa0_enabled)
    : m_hnd(i2c_handle) {
  if (sa0_enabled) {
    m_devAddress = 0x3D << 1;
  } else {
    m_devAddress = 0x3C << 1;
  }
}

SSD1306_I2C_Connector::~SSD1306_I2C_Connector() {}

bool SSD1306_I2C_Connector::send_cmd(uint8_t cmd) {
  HAL_StatusTypeDef status = HAL_I2C_Mem_Write(
      m_hnd, m_devAddress, 0x00, I2C_MEMADD_SIZE_8BIT, &cmd, 1, 1000);

  if (status == HAL_OK) return true;
  return false;
}

bool SSD1306_I2C_Connector::send_data(uint8_t* data, uint32_t size) {
  HAL_StatusTypeDef status = HAL_I2C_Mem_Write(
      m_hnd, m_devAddress, 0x40, I2C_MEMADD_SIZE_8BIT, data, size, 1000);
  if (status == HAL_OK) return true;
  return false;
}

bool SSD1306_I2C_Connector::isConnected() {
  HAL_StatusTypeDef status =
      HAL_I2C_IsDeviceReady(m_hnd, m_devAddress, 5, 1000);
  return status == HAL_OK;
}
