
#include "SSD1306DisplayAdapter.h"

#include <string.h>

uint32_t SSD1306DisplayAdapter::getHeight() const {
  return SSD1306DisplayAdapter::DisplayHeight;
}

void SSD1306DisplayAdapter::swap() {
  if (m_driver) {
    for (uint8_t i = 0; i < 8; i++) {
      m_driver->setPageStartAddress(i);
      m_driver->setLowerColumnStartAddress(0);
      m_driver->setHigherColumnStartAddress(0);

      m_driver->sendData(&m_drawBuffer[SSD1306DisplayAdapter::DisplayWidth * i],
                         SSD1306DisplayAdapter::DisplayWidth);
    }
  }
}

void SSD1306DisplayAdapter::drawPixel(uint32_t x, uint32_t y, uint32_t color) {
  if (SSD1306DisplayAdapter::DisplayWidth <= x ||
      SSD1306DisplayAdapter::DisplayHeight <= y) {
    return;
  }

  uint32_t address = SSD1306DisplayAdapter::DisplayWidth * (y / 8) + x;
  if (color) {
    m_drawBuffer[address] |= 1 << (y % 8);
  } else {
    m_drawBuffer[address] &= ~(1 << (y % 8));
  }
}

SSD1306DisplayAdapter::SSD1306DisplayAdapter(SSD1306_Driver* drv,
                                             bool destroy_driver)
    : m_driver(drv), m_destroy_driver(destroy_driver) {
  if (drv) {
    drv->setPowerMode(SSD1306_PowerState::Sleep);
    HAL_Delay(100);
    drv->setAddressingMode(SSD1306_AddressingMode::PageAddressing);
    drv->setPageStartAddress(0);
    drv->setCOMOutputScan(SSD1306_COMOutputScanMode::Remapped);
    drv->setLowerColumnStartAddress(0);
    drv->setHigherColumnStartAddress(0);
    drv->setStartLine(0);
    drv->setContrastLevel(0xFF);
    drv->setSegmentMapping(SSD1306_SegmentMapping::SEG0_To_127_Column);
    drv->setDisplayMode(SSD1306_DisplayMode::Normal);
    drv->setMultiplexRatio(63);
    drv->resumeDisplayRAMContent();
    drv->setDisplayOffset(0);
    drv->setClockDivideRatioAndOscillatorFreq(1, 15);
    drv->setPreChargePeriod(2, 2);
    drv->setCOMHardwareConfiguration(true, false);
    drv->setVCOMH_DeselectLevel(SSD1306_VCOMH_DeselectLevels::VCC_MUL_0_DOT_77);
    drv->enableChargePump();
    drv->setPowerMode(SSD1306_PowerState::Normal);
    HAL_Delay(100);
  }

  fill(0);
  swap();
}

SSD1306DisplayAdapter::~SSD1306DisplayAdapter() {
  if (m_driver) {
    m_driver->setPowerMode(SSD1306_PowerState::Sleep);
    HAL_Delay(100);

    if (m_destroy_driver) {
      delete m_driver;
      m_driver = nullptr;
    }
  }
}

void SSD1306DisplayAdapter::fill(uint32_t color) {
  if (color) {
    memset(m_drawBuffer, 0xFF, sizeof(m_drawBuffer));
  } else {
    memset(m_drawBuffer, 0x00, sizeof(m_drawBuffer));
  }
}

uint32_t SSD1306DisplayAdapter::getWidth() const {
  return SSD1306DisplayAdapter::DisplayWidth;
}
