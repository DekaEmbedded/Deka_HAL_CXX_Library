#include "Bitmap.h"

Bitmap::Bitmap() : Bitmap(nullptr, 0, 0) {}

Bitmap::Bitmap(const uint8_t* data, uint32_t w, uint32_t h)
    : m_data(data), m_width(w), m_height(h) {}

BitType Bitmap::get(uint32_t x, uint32_t y) const {
  uint32_t lineNumber = y / 8;
  uint32_t address = lineNumber * m_width + x;
  uint32_t shift = y % 8;

  return m_data[address] & (0b1 << shift);
}

uint32_t Bitmap::getWidth() const { return m_width; }

uint32_t Bitmap::getHeight() const { return m_height; }
