#pragma once

#include "system.h"

class IDisplayAdapter {
 public:
  IDisplayAdapter() = default;
  virtual ~IDisplayAdapter() = default;

  virtual uint32_t getWidth() const = 0;
  virtual uint32_t getHeight() const = 0;

  virtual void fill(uint32_t color) = 0;
  virtual void drawPixel(uint32_t x, uint32_t y, uint32_t color) = 0;
  virtual void swap() = 0;
};
