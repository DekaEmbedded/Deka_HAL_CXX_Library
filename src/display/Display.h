#pragma once

#include "Bitmap.h"
#include "IDisplayAdapter.h"
#include "IFont.h"

class Display {
 private:
  IDisplayAdapter* m_adapter;
  bool m_destroy_adapter;

 public:
  Display(IDisplayAdapter* adapter, bool destroy_adapter = false);
  ~Display();

  uint32_t getWidth() const;
  uint32_t getHeight() const;

  void fill(uint32_t color);
  void drawPixel(uint32_t x, uint32_t y, uint32_t color);
  void drawLine(uint32_t xb, uint32_t yb, uint32_t xe, uint32_t ye,
                uint32_t color);
  void drawBitmap(uint32_t x, uint32_t y, const Bitmap& bits, uint32_t color);

  uint32_t drawChar(uint32_t x, uint32_t y, IFont* font, CharType ch,
                    uint32_t color);
  uint32_t drawString(uint32_t x, uint32_t y, IFont* font, const CharType* str,
                      uint32_t color);

  void swap();
};
