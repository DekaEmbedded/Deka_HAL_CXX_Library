
#include "Display.h"

template <class T>
T clamp(T val, T min, T max) {
  if (max < val) return max;
  if (val < min) return min;
  return val;
}

Display::Display(IDisplayAdapter* adapter, bool destroy_adapter)
    : m_adapter(adapter), m_destroy_adapter(destroy_adapter) {}

Display::~Display() {
  if (m_destroy_adapter && m_adapter) {
    delete m_adapter;
    m_adapter = nullptr;
  }
}

uint32_t Display::getWidth() const {
  if (m_adapter) {
    return m_adapter->getWidth();
  }
  return 0;
}

uint32_t Display::getHeight() const {
  if (m_adapter) {
    return m_adapter->getHeight();
  }
  return 0;
}

void Display::fill(uint32_t color) {
  if (m_adapter) {
    m_adapter->fill(color);
  }
}

void Display::drawPixel(uint32_t x, uint32_t y, uint32_t color) {
  if (getWidth() < x || getHeight() < y) return;

  if (m_adapter) {
    m_adapter->drawPixel(x, y, color);
  }
}

void Display::drawLine(uint32_t xb, uint32_t yb, uint32_t xe, uint32_t ye,
                       uint32_t color) {
  if (!m_adapter) return;

  uint32_t w = m_adapter->getWidth();
  uint32_t h = m_adapter->getHeight();

  xb = clamp(xb, 0UL, w - 1);
  yb = clamp(yb, 0UL, h - 1);
  xe = clamp(xe, 0UL, w - 1);
  ye = clamp(ye, 0UL, h - 1);

  long long dx = xe - (long long)xb;
  long long dy = ye - (long long)yb;

  double coff = (dy / (double)dx);
  coff = (coff > 0) ? coff : -coff;
  bool useVertical = xb == xe || coff > 1;

  // If line is vertical
  if (useVertical) {
    long y_start = yb;
    long y_end = ye;
    if (ye < yb) {
      y_start = ye;
      y_end = yb;
    }

    for (long long y = y_start; y < y_end; y++) {
      long long x = (dx / (double)dy) * (y - yb) + xb;
      drawPixel(x, y, color);
    }
  } else {
    long x_start = xb;
    long x_end = xe;
    if (xe < xb) {
      x_start = xe;
      x_end = xb;
    }

    for (long long x = x_start; x < x_end; x++) {
      long long y = (dy / (double)dx) * (x - xb) + yb;
      drawPixel(x, y, color);
    }
  }
}

void Display::swap() {
  if (m_adapter) {
    m_adapter->swap();
  }
}

void Display::drawBitmap(uint32_t x, uint32_t y, const Bitmap& bits,
                         uint32_t color) {
  for (uint32_t xi = 0; xi < bits.getWidth(); xi++) {
    for (uint32_t yi = 0; yi < bits.getHeight(); yi++) {
      if (bits.get(xi, yi)) {
        drawPixel(x + xi, y + yi, color);
      }
    }
  }
}

uint32_t Display::drawChar(uint32_t x, uint32_t y, IFont* font, CharType ch,
                           uint32_t color) {
  Bitmap char_bitmap = font->getCharBitmap(ch);
  drawBitmap(x, y, char_bitmap, color);
  return char_bitmap.getWidth();
}

uint32_t Display::drawString(uint32_t x, uint32_t y, IFont* font,
                             const CharType* str, uint32_t color) {
  uint32_t size = 0;
  CharType* s = (CharType*)str;
  while (*s != 0) {
    size += drawChar(x + size, y, font, *s, color);

    s++;
  }

  return size;
}
