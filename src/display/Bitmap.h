#pragma once

#include "system.h"

using BitType = uint8_t;

class Bitmap {
 private:
  const uint8_t* m_data;

  uint32_t m_width;
  uint32_t m_height;

 public:
  Bitmap();
  Bitmap(const uint8_t* data, uint32_t w, uint32_t h);

  BitType get(uint32_t x, uint32_t y) const;

  uint32_t getWidth() const;
  uint32_t getHeight() const;
};
