#pragma once

#include "../ITemperatureSensor.h"
#include "system.h"

class LM75B_Sensor : public ITemperatureSensor {
 private:
  I2C_HandleTypeDef* m_i2c;
  uint8_t m_slave_address;

 public:
  LM75B_Sensor(I2C_HandleTypeDef* i2c, uint8_t slaveBits);
  ~LM75B_Sensor() override;

  float getTemperatureStep() const override;
  float getMinTemperature() const override;
  float getTemperature() const override;
  float getMaxTemperature() const override;
};
