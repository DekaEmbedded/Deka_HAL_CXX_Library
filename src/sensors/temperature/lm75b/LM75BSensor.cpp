
#include "LM75BSensor.h"

LM75B_Sensor::LM75B_Sensor(I2C_HandleTypeDef* i2c, uint8_t slaveBits)
    : m_i2c(i2c) {
  m_slave_address = 0b01001000;
  m_slave_address |= (slaveBits & 0b00000111);
  m_slave_address <<= 1;
}

LM75B_Sensor::~LM75B_Sensor() {}

float LM75B_Sensor::getTemperatureStep() const { return 0.125f; }

float LM75B_Sensor::getMinTemperature() const { return -55.0f; }

float LM75B_Sensor::getTemperature() const {
  uint8_t data[2] = {0};
  HAL_I2C_Mem_Read(m_i2c, m_slave_address, 0x00, I2C_MEMADD_SIZE_8BIT, data, 2,
                   100);

  float v = ((data[0] * 8.0) + (data[1] >> 5)) * getTemperatureStep();
  return v;
}

float LM75B_Sensor::getMaxTemperature() const { return +125.0f; }
