#pragma once

#include "../ISensor.hpp"

class ITemperatureSensor : public ISensor {
public:
    ITemperatureSensor() = default;
    virtual ~ITemperatureSensor() override = default;

    virtual float getTemperature() const = 0;

    virtual float getMinTemperature() const = 0;
    virtual float getMaxTemperature() const = 0;
    virtual float getTemperatureStep() const = 0;
};
