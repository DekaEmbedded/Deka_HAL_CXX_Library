#include "adxl345_i2c.hpp"

ADXL345_I2C::ADXL345_I2C(I2C_HandleTypeDef* hnd, ADXL345_Config config)
    : m_validated(false)
    , m_i2c(hnd)
{
    if (config.is_alt_address) {
        m_address = 0x1D << 1;
    } else {
        m_address = 0x53 << 1;
    }

    uint8_t devID = 0;
    HAL_I2C_Mem_Read(m_i2c, m_address, 0x00, I2C_MEMADD_SIZE_8BIT, &devID, 1, 100);

    if (devID == 0xE5) {
        m_validated = true;

        uint8_t mode = 0b00000000;
        mode |= ((uint8_t)(config.range));

        HAL_I2C_Mem_Write(m_i2c, m_address, 0x31, I2C_MEMADD_SIZE_8BIT, &mode, 1, 100); //Write to mode

        switch (config.range) {
        case ADXL345_Ranges::Range_2G:
            m_modif = 3.9 / 1000;
            break;

        case ADXL345_Ranges::Range_4G:
            m_modif = 7.8 / 1000;
            break;

        case ADXL345_Ranges::Range_8G:
            m_modif = 15.6 / 1000;
            break;

        case ADXL345_Ranges::Range_16G:
            m_modif = 31.2 / 1000;
            break;

        default:
            break;
        }
    }
}

ADXL345_I2C::~ADXL345_I2C()
{
    stopMeasure();
}

bool ADXL345_I2C::startMeasure()
{
    if (!m_validated)
        return false;

    uint8_t new_pwr_mode = 0b00001000;
    HAL_I2C_Mem_Write(m_i2c, m_address, 0x2D, I2C_MEMADD_SIZE_8BIT, &new_pwr_mode, 1, 100);
}

void ADXL345_I2C::stopMeasure()
{
    if (!m_validated)
        return;

    uint8_t new_pwr_mode = 0b00000000;
    HAL_I2C_Mem_Write(m_i2c, m_address, 0x2D, I2C_MEMADD_SIZE_8BIT, &new_pwr_mode, 1, 100);
}

AccelerometerVec3 ADXL345_I2C::getAcceleration()
{

    AccelerometerVec3 out = { 0, 0, 0 };

    if (!m_validated) {
        return out;
    }

    uint8_t data[6];

    HAL_I2C_Mem_Read(m_i2c, m_address, 0x32, I2C_MEMADD_SIZE_8BIT, data, 6, 100);

    int rawX = ((int8_t)(data[1] << 8) | (int8_t)data[0]);
    int rawY = ((int8_t)(data[3] << 8) | (int8_t)data[2]);
    int rawZ = ((int8_t)(data[5] << 8) | (int8_t)data[4]);

    out.x = (float)(rawX)*m_modif;
    out.y = (float)(rawY)*m_modif;
    out.z = (float)(rawZ)*m_modif;

    return out;
}