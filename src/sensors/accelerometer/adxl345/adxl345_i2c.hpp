#pragma once

#include "../IAccelerometer.hpp"
#include "system.h"

enum class ADXL345_Ranges {
    Range_2G = 0b00,
    Range_4G = 0b01,
    Range_8G = 0b10,
    Range_16G = 0b11
};

struct ADXL345_Config {
    bool is_alt_address = false;
    ADXL345_Ranges range = ADXL345_Ranges::Range_2G;
};

class ADXL345_I2C : public IAccelerometer {
private:
    bool m_validated;
    I2C_HandleTypeDef* m_i2c;

    uint8_t m_address;
    float m_modif;

public:
    ADXL345_I2C(I2C_HandleTypeDef* hnd, ADXL345_Config config);
    ~ADXL345_I2C();

    bool startMeasure() override;
    void stopMeasure() override;

    AccelerometerVec3 getAcceleration() override;
};