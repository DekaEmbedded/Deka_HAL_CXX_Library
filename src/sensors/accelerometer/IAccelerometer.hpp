#pragma once

#include "../ISensor.hpp"

struct AccelerometerVec3 {
    float x;
    float y;
    float z;
};

class IAccelerometer : public ISensor {
public:
    IAccelerometer() = default;
    ~IAccelerometer() override = default;

    virtual AccelerometerVec3 getAcceleration();
};