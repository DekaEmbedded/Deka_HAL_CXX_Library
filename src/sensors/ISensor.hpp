#pragma once

class ISensor {
public:
    ISensor() = default;
    virtual ~ISensor() = default;

    virtual bool startMeasure();
    virtual void stopMeasure();
};
