#pragma once

#include "../IRealTimeClock.h"
#include "system.h"

class DS3231_RTC : public IRealTimeClock {
 private:
  I2C_HandleTypeDef* m_i2c;
  uint8_t m_slave_address;

 public:
  DS3231_RTC(I2C_HandleTypeDef* i2c);
  ~DS3231_RTC() override = default;

  uint8_t getHours() const override;
  uint8_t getSeconds() const override;
  uint8_t getDayOfWeek() const override;
  uint16_t getYear() const override;
  uint8_t getDayOfMonth() const override;
  uint8_t getMonth() const override;
  uint8_t getMinutes() const override;

  void setMonth(uint8_t new_month) const override;
  void setSeconds(uint8_t new_seconds) const override;
  void setDayOfWeek(uint8_t new_dow) const override;
  void setYear(uint16_t new_year) const override;
  void setDayOfMonth(uint8_t new_dom) const override;
  void setHours(uint8_t new_hours) const override;
  void setMinutes(uint8_t new_minutes) const override;
};
