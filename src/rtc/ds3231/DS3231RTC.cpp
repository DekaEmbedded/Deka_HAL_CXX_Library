/*
 * DS3231RTC.cpp
 *
 *  Created on: 22 ���. 2020 �.
 *      Author: Deucalion
 */

#include "DS3231RTC.h"

DS3231_RTC::DS3231_RTC(I2C_HandleTypeDef* i2c)
    : m_i2c(i2c), m_slave_address(0x68 << 1) {}

uint8_t DS3231_RTC::getHours() const {
  uint8_t hours = 0;
  HAL_I2C_Mem_Read(m_i2c, m_slave_address, 0x02, I2C_MEMADD_SIZE_8BIT, &hours,
                   1, 100);

  uint8_t real_hours = (hours & 0b00001111);
  if (hours & 0b00010000)  // Bit 10 hour enabled
  {
    real_hours += 10;
  }
  if (hours & 0b00100000)  // Bit 20 hour enabled
  {
    real_hours += 20;
  }

  return real_hours;
}

uint8_t DS3231_RTC::getSeconds() const {
  uint8_t seconds = 0;
  HAL_I2C_Mem_Read(m_i2c, m_slave_address, 0x00, I2C_MEMADD_SIZE_8BIT, &seconds,
                   1, 100);

  uint8_t real_seconds = (seconds & 0b00001111);
  real_seconds += ((seconds & 0b01110000) >> 4) * 10;

  return real_seconds;
}

uint8_t DS3231_RTC::getDayOfWeek() const {
  uint8_t dow = 0;
  HAL_I2C_Mem_Read(m_i2c, m_slave_address, 0x03, I2C_MEMADD_SIZE_8BIT, &dow, 1,
                   100);

  return dow;
}

uint16_t DS3231_RTC::getYear() const {
  const uint16_t base_year = 2000;
  uint8_t year = 0;
  HAL_I2C_Mem_Read(m_i2c, m_slave_address, 0x06, I2C_MEMADD_SIZE_8BIT, &year, 1,
                   100);

  uint16_t real_year = base_year + (year & 0b00001111);
  real_year += ((year & 0b11110000) >> 4) * 10;

  return real_year;
}

uint8_t DS3231_RTC::getDayOfMonth() const {
  uint8_t day = 0;
  HAL_I2C_Mem_Read(m_i2c, m_slave_address, 0x04, I2C_MEMADD_SIZE_8BIT, &day, 1,
                   100);

  uint8_t real_day = (day & 0b00001111);
  real_day += ((day & 0b00110000) >> 4) * 10;

  return real_day;
}

uint8_t DS3231_RTC::getMonth() const {
  uint8_t month = 0;
  HAL_I2C_Mem_Read(m_i2c, m_slave_address, 0x05, I2C_MEMADD_SIZE_8BIT, &month,
                   1, 100);

  uint8_t real_month = (month & 0b00001111);
  real_month += ((month & 0b00010000) >> 4) * 10;

  return real_month;
}

uint8_t DS3231_RTC::getMinutes() const {
  uint8_t minutes = 0;
  HAL_I2C_Mem_Read(m_i2c, m_slave_address, 0x01, I2C_MEMADD_SIZE_8BIT, &minutes,
                   1, 100);

  uint8_t real_minutes = (minutes & 0b00001111);
  real_minutes += ((minutes & 0b01110000) >> 4) * 10;

  return real_minutes;
}

void DS3231_RTC::setMonth(uint8_t new_month) const {
  uint8_t month_10 = (new_month / 10);
  uint8_t month_1 = new_month - month_10 * 10;

  uint8_t toDevice = (month_10 << 4) | month_1;

  HAL_I2C_Mem_Write(m_i2c, m_slave_address | 1, 0x05, I2C_MEMADD_SIZE_8BIT,
                    &toDevice, 1, 100);
}

void DS3231_RTC::setSeconds(uint8_t new_seconds) const {
  uint8_t seconds_10 = (new_seconds / 10);
  uint8_t seconds_1 = new_seconds - seconds_10 * 10;

  uint8_t toDevice = (seconds_10 << 4) | seconds_1;

  HAL_I2C_Mem_Write(m_i2c, m_slave_address | 1, 0x00, I2C_MEMADD_SIZE_8BIT,
                    &toDevice, 1, 100);
}

void DS3231_RTC::setDayOfWeek(uint8_t new_dow) const {
  HAL_I2C_Mem_Write(m_i2c, m_slave_address | 1, 0x03, I2C_MEMADD_SIZE_8BIT,
                    &new_dow, 1, 100);
}

void DS3231_RTC::setYear(uint16_t new_year) const {
  uint8_t year_high = (new_year / 100);
  uint8_t year_10 = (new_year / 10) - year_high * 10;
  uint8_t year_1 = new_year - year_10 * 10 - year_high * 100;

  uint8_t toDevice = (year_10 << 4) | year_1;

  HAL_I2C_Mem_Write(m_i2c, m_slave_address | 1, 0x06, I2C_MEMADD_SIZE_8BIT,
                    &toDevice, 1, 100);
}

void DS3231_RTC::setDayOfMonth(uint8_t new_dom) const {
  uint8_t dom_10 = (new_dom / 10);
  uint8_t dom_1 = new_dom - dom_10 * 10;

  uint8_t toDevice = (dom_10 << 4) | dom_1;

  HAL_I2C_Mem_Write(m_i2c, m_slave_address | 1, 0x04, I2C_MEMADD_SIZE_8BIT,
                    &toDevice, 1, 100);
}

void DS3231_RTC::setHours(uint8_t new_hours) const {
  uint8_t hour_10 = (new_hours / 10);
  uint8_t hour_1 = new_hours - hour_10 * 10;

  uint8_t toDevice = (hour_10 << 4) | hour_1;

  HAL_I2C_Mem_Write(m_i2c, m_slave_address | 1, 0x02, I2C_MEMADD_SIZE_8BIT,
                    &toDevice, 1, 100);
}

void DS3231_RTC::setMinutes(uint8_t new_minutes) const {
  uint8_t minutes_10 = (new_minutes / 10);
  uint8_t minutes_1 = new_minutes - minutes_10 * 10;

  uint8_t toDevice = (minutes_10 << 4) | minutes_1;

  HAL_I2C_Mem_Write(m_i2c, m_slave_address | 1, 0x01, I2C_MEMADD_SIZE_8BIT,
                    &toDevice, 1, 100);
}
