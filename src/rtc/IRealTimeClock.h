#pragma once

#include "system.h"

class IRealTimeClock {
 public:
  IRealTimeClock() = default;
  virtual ~IRealTimeClock() = default;

  virtual uint8_t getSeconds() const = 0;
  virtual uint8_t getMinutes() const = 0;
  virtual uint8_t getHours() const = 0;

  virtual uint8_t getDayOfWeek() const = 0;

  virtual uint8_t getDayOfMonth() const = 0;
  virtual uint8_t getMonth() const = 0;
  virtual uint16_t getYear() const = 0;

  virtual void setSeconds(uint8_t new_seconds) const = 0;
  virtual void setMinutes(uint8_t new_minutes) const = 0;
  virtual void setHours(uint8_t new_hours) const = 0;

  virtual void setDayOfWeek(uint8_t new_dow) const = 0;

  virtual void setDayOfMonth(uint8_t new_dom) const = 0;
  virtual void setMonth(uint8_t new_month) const = 0;
  virtual void setYear(uint16_t new_year) const = 0;
};
