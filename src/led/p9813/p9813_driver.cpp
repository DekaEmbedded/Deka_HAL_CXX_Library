#include "p9813_driver.h"

P9813_Driver::P9813_Driver(GPIO_TypeDef *gpio_clk, uint16_t gpio_pin_clk,
                           GPIO_TypeDef *gpio_data, uint16_t gpio_pin_data)
    : m_gpio_clk(gpio_clk),
      m_gpio_pin_clk(gpio_pin_clk),
      m_gpio_data(gpio_data),
      m_gpio_pin_data(gpio_pin_data) {
  // Init gpio for clock
  GPIO_InitTypeDef clockInitPin = GPIO_InitTypeDef();
  clockInitPin.Pin = gpio_pin_clk;
  clockInitPin.Mode = GPIO_MODE_OUTPUT_PP;
  clockInitPin.Speed = GPIO_SPEED_FAST;

  HAL_GPIO_Init(gpio_clk, &clockInitPin);

  // Init gpio for data
  GPIO_InitTypeDef dataInitPin = GPIO_InitTypeDef();
  dataInitPin.Pin = gpio_pin_data;
  dataInitPin.Mode = GPIO_MODE_OUTPUT_PP;
  dataInitPin.Speed = GPIO_SPEED_FAST;

  HAL_GPIO_Init(gpio_data, &dataInitPin);
}

P9813_Driver::~P9813_Driver() {
  HAL_GPIO_DeInit(m_gpio_clk, m_gpio_pin_clk);
  HAL_GPIO_DeInit(m_gpio_data, m_gpio_pin_data);
}

void P9813_Driver::setColor(uint8_t r, uint8_t g, uint8_t b) {
  sendData(0);

  auto data = generateDataForRGB(r, g, b);
  sendData(data);

  sendData(0);
}

uint32_t P9813_Driver::generateDataForRGB(uint8_t r, uint8_t g, uint8_t b) {
  uint32_t out = 0;

  uint8_t prefix = 0b11000000;

  prefix |= (~(b >> 6) & 0b11) << 4;  // Blue
  prefix |= (~(g >> 6) & 0b11) << 2;
  prefix |= (~(r >> 6) & 0b11);

  out = (prefix << 24);  // Prefix
  out |= (b << 16);      // Blue
  out |= (g << 8);       // Green
  out |= (r << 0);       // Red

  return out;
}

void P9813_Driver::sendData(uint32_t data) {
  const uint32_t upperBitMask = 0x80000000;

  for (int i = 0; i < 32; i++) {
    uint32_t val = data & upperBitMask;
    if (val) {
      HAL_GPIO_WritePin(m_gpio_data, m_gpio_pin_data, GPIO_PIN_SET);
    } else {
      HAL_GPIO_WritePin(m_gpio_data, m_gpio_pin_data, GPIO_PIN_RESET);
    }
    clock();
    data <<= 1;
  }
}

void P9813_Driver::clock() {
  HAL_GPIO_WritePin(m_gpio_clk, m_gpio_pin_clk, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(m_gpio_clk, m_gpio_pin_clk, GPIO_PIN_SET);
}
