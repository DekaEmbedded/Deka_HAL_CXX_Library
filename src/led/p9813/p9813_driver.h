#pragma once

#include "system.h"

class P9813_Driver {
private:
	GPIO_TypeDef* m_gpio_clk;
	uint16_t m_gpio_pin_clk;
	GPIO_TypeDef* m_gpio_data;
	uint16_t m_gpio_pin_data;
public:
	P9813_Driver(GPIO_TypeDef* gpio_clk, uint16_t gpio_pin_clk, GPIO_TypeDef* gpio_data, uint16_t gpio_pin_data);
	~P9813_Driver();

	void setColor(uint8_t r, uint8_t g, uint8_t b);

private:
	uint32_t generateDataForRGB(uint8_t r, uint8_t g, uint8_t b);
	void sendData(uint32_t data);
	void clock();
};
